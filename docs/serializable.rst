Serializable Forms
==================

Serializable forms of various classes.  

:class:`datetime.datetime`
--------------------------

:class:`dict` with following keys.  

*  ``__type__``

   :content:
      ``'datetime'``
   
*  ``timestamp``

   :type:
      :class:`float`

   :content:
      Floating-point unix timestamp.  

For example::
   
   {'timestamp': 1376239603.0, '__type__': 'datetime'}

And it can be encoded to JSON like::
    
   {"timestamp": 1376239603.0, "__type__": "datetime"}

:class:`ybs.forms.Form`
-------------------------

:class:`dict` with following keys.  

*  ``__type__``

   :content:
      ``'ybs.forms.<FormName>'``

*  ``error``
   
   :type:
      :class:`str`

   :content:
      Form-level error

*  ``fields``

   :type:
      :class:`dict`

   :content:
      Key is name of field.  

      And value is :class:`dict` with following keys.  

      -  ``data``

          Field's value that user gave.  

      -  ``error``

          Error message of the field.  

:class:`sqlalchemy.entity.Image`
--------------------------------

:class:`dict` with following keys.

*  ``__type__``
   
   :content:
      ``'Image'``

*  ``width``
   
   :type:
      :class:`int`

   :content:
      Width of image

*  ``height``

   :type:
      :class:`int`

   :content:
      Height of image

*  ``url``
   
   :type:
      :class:`str`

   :content:
      URL of image

:class:`ybs.contrib.ext.sqlalchemy_imageattach_ext.CommonImageSet`
--------------------------------------------------------------------

:class:`dict` with following keys.

*  ``__type__``
   
   :content:
       ``'ImageSet'``

*  ``original``
   
   :type:
      serialized :class:`~sqlalchemy.entity.Image`

      Seralizable form is described in `sqlalchemy.entity.Image`_

   :content:
      Original or largest image.  It can be null

*   ``images``
   
   :type:
      :class:`dict`

   :content:
      Dictionary of scaled image.  

      Key is scaled image's key, value is :class:`~sqlalchemy.entity.Image` or
      :const:`None`

      Seralizable form is described in `sqlalchemy.entity.Image`_

      Keys

      -  large

      -  medium

      -  small

*  ``thumbnails``

   :type:
      :class:`dict`

   :content:
      Dictionary of thumbnails.  

      Key is thumbnail's key, value is :class:`~sqlalchemy.entity.Image` or
      :const:`None`

      Seralizable form is described in `sqlalchemy.entity.Image`_

      Keys

      -  large

      -  medium

      -  small


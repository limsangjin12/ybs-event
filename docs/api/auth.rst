Authentication API
==================

로그인은 HTTP Cookie를 이용한 방식을 사용하며 Session expiration 기간은 30일 
입니다.  
또한 Basic Auth도 지원하여 Debug시에 유용하게 사용할 수 있습니다.  

.. autoflask:: dummyapp:app
   :blueprints: auth
   :undoc-static:

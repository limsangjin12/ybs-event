.. YBS documentation master file, created by
   sphinx-quickstart on Fri Aug  9 00:40:18 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to YBS's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 1

   reference/modules
   rest

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Basic Concept
=============

YBS의 REST API 서버의 Basic Concept입니다. 기본적인 패턴과 네이밍 컨벤션을
정의하며 가능한한 본 페이지의 내용을 준수할 것 입니다.

As RESTful as Possible
----------------------

본 서버는 가능한한 HTTP_ 표준을 준수하며 `RESTful`__ HTTP Server를 지향합니다.
필요한 Specification의 대부분을 HTTP에 의존하여 작성할 것입니다.  특히 중점을
두어 사용하는 HTTP Spec은 다음과 같습니다.

__ REST_

CRUD by HTTP Method 
~~~~~~~~~~~~~~~~~~~

데이터의 ``Create``, ``Read``, ``Update``, ``Delete`` (CRUD)를 HTTP Method에
매칭하여 사용하며 URL은 데이터의 종류를 Representation 하는데에 사용됩니다.  
이러한 특성으로인해 같은 URL이라도 HTTP Method에 따라 다른 Action이 일어날 수
있으니 주의해야 합니다.

HTTP Method와 CRUD는 다음과 같이 매칭됩니다.

+-----------------------+-----------------------+
| HTTP Method           | Data                  |
+=======================+=======================+
| :http:method:`get`    | Read                  |
+-----------------------+-----------------------+
| :http:method:`post`   | Create                |
+-----------------------+-----------------------+
| :http:method:`put`    | Update                |
+-----------------------+-----------------------+
| :http:method:`delete` | Delete                |
+-----------------------+-----------------------+

:http:method:`options`, :http:method:`head` 등은 사용하지 않습니다.  

HTML Support
~~~~~~~~~~~~

HTML_ 은 :http:method:`get` 과 :http:method:`post` 외에는 지원하지 않습니다.  

이런 특징 때문에 REST API를 설계하는데에 한계가 있어 다른 HTTP Method를
지원하기 위해 HTTP Method를 URL에 Parameterize했습니다.  

URL의 Query String에 ``__method__`` 라는 Key가 있다면 그 Key에 해당하는 Value를
HTTP Method로 사용하게 됩니다.  

예를 들어 HTML_ 에서 ``/foo/`` 라는 URL에 :http:method:`delete` request를
보내기 위해선 ``/foo/?__method__=DELETE`` 로 Request를 보내야 합니다.  

또한 Method의 변환은 WSGI_ Middleware에서 이루어지게 됩니다.  

Data are Represented by URL
~~~~~~~~~~~~~~~~~~~~~~~~~~~

`CRUD by HTTP Method`_ 에서 언급한 바와 같이 URL은 Data를 Represent 하는데에
사용됩니다.  

Foo라는 종류의 데이터 Set의 경우 URL ``/foo/`` 를 기본으로 하여
URL ``/foo/<foo_id>`` 로 데이터를 선택할 수 있습니다. 

예를 들어 URL ``/foo/``
에 :http:method:`get` Method를 사용하여 Request를 보내면 데이터 Foo의 리스트를
반환합니다.  

또, URL ``/foo/815`` 에 :http:method:`get` 을 사용하여 Request를 보내면 ID가
815인 단일 Foo 데이터를 반환합니다.

조금 더 자세한 예를 들면 다음과 같이 정리할 수 있습니다.

+---------------+-----------------------+-------------------------------------+
| URL           | Method                | Action                              |
+===============+=======================+=====================================+
| /foo/         | :http:method:`get`    | Get list of Foo                     |
+---------------+-----------------------+-------------------------------------+
| /foo/<foo_id> | :http:method:`get`    | Get a Foo whose ID is ``foo_id``.   |
+---------------+-----------------------+-------------------------------------+
| /foo/         | :http:method:`post`   | Create a new Foo.                   |
+---------------+-----------------------+-------------------------------------+
| /foo/<foo_id> | :http:method:`put`    | Update contents of Foo whose ID is  |
|               |                       | ``foo_id``.                         |
+---------------+-----------------------+-------------------------------------+
| /foo/<foo_id> | :http:method:`delete` | Delete a Foo whose ID is            |
|               |                       | ``foo_id``.                         |
+---------------+-----------------------+-------------------------------------+
| /foo/         | :http:method:`delete` | Delete all Foo (maybe with some     |
|               |                       | filters).                           |
+---------------+-----------------------+-------------------------------------+

추가로 ``/foo/<foo_id>/bar/`` 의 경우에는 ID가 ``foo_id`` Foo에 종속된
Bar데이터를 의미합니다.  예를 들면 ``/story/1229/comment/`` 가 있습니다.

Content Negotiation
~~~~~~~~~~~~~~~~~~~

YBS 서버는 Content Negotiation을 지원합니다. 

`Content Negotiation`__ 은 HTTP Request의 ``Accept`` 헤더
필드의 값에 따라 다른 `MIME Type`__ 으로 데이터를 반환하는 것으로 ``text/html``
은 HTML, ``application/json`` 은 JSON_ 을 나타냅니다.  

이 문서가 작성되는 시점인 2013년 8월 12일 오후 8시 15분 (KST)에는 대부분의
URL이 JSON_ 데이터만 지원합니다.  그렇지만 ``Accept`` 헤더 필드에
``application/json`` 을 정확히 명시하지 않으면 ``406: Not Acceptable`` HTTP
에러가 발생할 수 있으니 반드시 명시해 주시기 바랍니다.  

__ Content_Negotiation_
__ MIME_TYPE_

HTTP Status Code
~~~~~~~~~~~~~~~~

`HTTP Status Code`__ 는 항상 중요한 의미를 가지며 주로 사용되는 status code는
다음과 같습니다.

*  ``200`` 

   Done

*  ``201``

   Created

*  ``204``
   
   No Content (삭제가 성공했을시에 사용됩니다.)

*  ``400``
   
   Bad Request (대부분의 경우 에러에 대한 Description을 포함합니다.)

*  ``401``
   
   Unauthorized (대부분의 경우 로그인이 필요함을 의미합니다.)

*  ``403``

   Forbidden (대부분의 경우 접근할 수 없는 데이터에 접근함을 의미합니다.)

*  ``405``
   
   Method Not Allowed

*  ``406``
   
   Not Acceptable (대부분의 경우 잘못된 ``Accept`` 헤더 필드가 잘못되었음을
   의미합니다.)

*  ``500``

   Internal Server Error (서버 에러입니다. 발생시 보고해주세요.)

200 ~ 299는 성공으로 처리하고 400 ~ 599는 에러로 처리하셔도 좋습니다.  

__ HTTP_Status_Code_

Data Serialization
------------------

Data를 Client로 전달하기 위해 서버는 Python Object를 Serialize 하며 주로
사용되는 형태는 JSON_ 이 될 것입니다.  Serialization의 자동화와 형태의 일관성을
위해서 서버에서는 Type에 따라 일정한 형식으로 Data를 JSON Serializable한 형태로
변환합니다.

또한 JSON_ 형식의 경우 Root Object는 Dictionary이며 ``data`` Key의 Value가 
반환되는 Object입니다.  

예를들어 String의 List를 반환한다면::

   {
        "data": [
            "str1",
            "str2"
        ]
   }

위와 같은 JSON_ 이 전달될 것입니다.  

JSON Serializable한 Type은 다음과 같습니다.

*  :class:`int`, :class:`long`, :class:`float` ...

   숫자 Type 입니다.

*  :class:`dict`

   Dictionary Type입니다. Java_ 의 ``java.util.Map`` 과 Equivalent합니다. 

*  :class:`list`
   
   List Type입니다. Java_ 의 ``java.util.List`` 와 Equivalent합니다.  

*  :class:`str`, :class:`unicode`
   
   String Type입니다. Java_ 의  ``java.lang.String`` 과 Equivalent합니다.  

위에 나열된 Type들은 JSON_ 의 기본 형태를 따르며 위에 포함되지 않는 Type은 
Dictionary Type으로 변환되며 ``__type__`` 이라는 특수한 Key를 포함합니다.  
이때 Dictionary의 다른 Key들의 Dictionary의 Key를 의미하는것이 아닌 Data의
속성을 의미하게 됩니다.  ORM 모델의 경우에는 대부분 컬럼의 값이 될 것입니다.  
다양한 Type의 Serialization Form은 :doc:`serializable` 에서 확인할 수 있으며 ORM
모델은 각 모델에 같이 명시하고 있습니다. (:mod:`ybs.models`) 

Example
~~~~~~~

다음과 같은 Python Class가 있다고 가정합니다::

   class Foo(object):
       bar = 'name'
       baz = datetime.datetime(2013, 8, 12, 21, 1, 29, 892034)

그리고 Serialization Form은 다음과 같습니다::

   {
       '__type__':'Foo',
       'bar':...,
       'baz':...
   }

그렇다면 다음과 같은 Data는::

   {
      'foo':Foo(),
      'items':[1, 2, 3]
   }

다음과 같이 JSON Serialize됩니다::
   
   {
       "items": [
           1,
           2,
           3
       ],
       "foo": {
           "bar": "name",
           "__type__": "Foo",
           "baz": {
               "timestamp": 1376308889.0,
               "__type__": "datetime"
            }
       }
   }

그러면 Client에선 다음과 같은 Converter를 작성할 수 있습니다::

   import datetime

   def convert(item):
       if isinstance(item, dict):
           return convert_dict(item)
       elif isinstance(item, list):
           return convert_list(item)
       else:
           return item

   def convert_dict(item):
       if item.has_key('__type__'):
           type = item['__type__']
           if type == 'Foo':
               return convert_foo(item)
           elif type == 'datetime':
               return convert_datetime(item)
       converted = {}
       for k, v in item.items():
           converted[k] = convert(v)

   def convert_list(item):
       return map(convert, item)

   def convert_foo(item):
       foo = Foo()
       foo.bar = convert(item['bar'])
       foo.baz = convert(item['baz'])
       return foo

   def convert_datetime(item):
       return datetime.datetime.fromtimestamp(item['timestamp'])

그리고 다음과 같이 사용할 수 있습니다::
   
   import json
   from converter import convert

   json_data = json.loads(load_data())
   data = convert(json_data['data'])

.. _HTML: http://en.wikipedia.org/wiki/HTML
.. _WSGI: http://www.python.org/dev/peps/pep-0333/
.. _HTTP: http://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol
.. _REST: http://en.wikipedia.org/wiki/Representational_state_transfer
.. _HTTP_Status_Code: http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
.. _Content_Negotiation: http://en.wikipedia.org/wiki/Content_negotiation
.. _JSON: http://www.json.org/
.. _MIME_TYPE: http://en.wikipedia.org/wiki/Internet_media_type
.. _JAVA: http://java.com/


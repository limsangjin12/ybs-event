REST API
========

YBS API 서버의 REST API 문서입니다.  다른 문서에 앞서 :doc:`basic_concept` 를
먼저 읽어 보시기를 권장합니다.  

.. toctree::
   :maxdepth: 1

   basic_concept.rst
   serializable
   api/index.rst


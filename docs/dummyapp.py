from ybs.app import create_app

app = create_app({
    'DEBUG':False,
    'SECRET_KEY':'doc',
    'DATABASE':'sqlite://',
    'ADMIN':False,
})

import config
from ybs.app import create_app

if __name__ == '__main__':
    app = create_app(config)
    app.celery.worker_main()

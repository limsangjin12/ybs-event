from setuptools import setup
import setuptools
import os
import sys
sys.path.insert(1, os.path.abspath('docs'))

requires = [
    'Flask>=0.9',
    'Flask-Negotiation',
    'Flask-Admin',
    'SQLAlchemy',
    'wand>=0.3.0',
    'psycopg2',
    'SQLAlchemy-ImageAttach>=0.8.0',
    'Formencode',
    'Formencode-Jinja2',
    'Celery>=3.0.0',
# Documentation Requirements
    'sphinxcontrib-httpdomain',
]

dependency_links=[]

setup(
    name='Epicl',
    version='0.1.0',
    author='GunWoo Choi',
    author_email='6566gun@gmail.com',
    description='Epicl server',
    long_description=__doc__,
    packages=setuptools.find_packages(),
    include_package_data=True,
    zip_safe=False,
    platforms='any',
    install_requires=requires,
    dependency_links=dependency_links,
    classifiers=[
        'Environment :: Web Environment',
    ],
)

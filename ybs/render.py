import errors
import datetime
import formencode
import forms as f
import models as m

from json import JSONEncoder
from flask import request
from contrib.utils import timestamp
from contrib.typeconverter import Converter
from contrib.ext.sqlalchemy_imageattach_ext import CommonImageSet
from flask.ext.negotiation import Render
from flask.ext.negotiation.renderers import Renderer, template_renderer
from flask.ext.negotiation.media_type import (acceptable_media_types, MediaType,
    choose_media_type)
from sqlalchemy_imageattach.entity import Image
from werkzeug.datastructures import FileStorage

def wants_html():
    media_types = []
    for renderer in render.renderers:
        media_types += renderer.media_types
    acceptables = acceptable_media_types(request)
    acceptable = choose_media_type(acceptables, media_types)
    return MediaType('text/html') in acceptable


class ConvertEncoder(JSONEncoder):
    def default(form, obj):
        return converter.convert(obj)

class DeepConverter(Converter):
    def assert_type(form, obj):
        super(DeepConverter, form).assert_type(obj)
        if isinstance(obj, list):
            for i in obj:
                form.assert_type(i)
        elif isinstance(obj, dict):
            for k, v in obj.iteritems():
                form.assert_type(k)
                form.assert_type(v)

converter = DeepConverter(
    (list, dict, long, int, float, str, unicode, type(None))
)
convert_encoder = ConvertEncoder(converter)

class ConvertJSONRenderer(Renderer):
    __media_types__ = ('application/json', )
    def render(form, data, template=None, ctx=None):
        return convert_encoder.encode({'data':data})

render = Render(renderers=(ConvertJSONRenderer(), template_renderer))
""" Renderer is here!
"""

@converter.handle(Image)
def convert_image(image):
    return {
        '__type__':'Image',
        'width':image.width,
        'height':image.height,
        'url':image.locate(),
    }

@converter.handle(m.Base)
def convert_model(model):
    d = {
        '__type__':m.__name__ + '.' + type(model).__name__,
    }
    attrs = set([x.name for x in model.__table__.columns])
    withs = set(getattr(model, '__with__', ()))
    exs = set(getattr(model, '__ex__', ()))
    attrs |= withs
    attrs -= exs
    for attr in attrs:
        x = object()
        value = getattr(model, attr, x)
        if value is x:
            continue
        d[attr] = converter.convert(value)
    return d

@converter.handle(CommonImageSet)
def convert_imageset(imageset):
    original = imageset.original
    thumbnails = {}
    images = {}
    image_sizes = list(imageset.__sizes__)
    for image_key, (width, height) in image_sizes:
        image = imageset.get_scaled_image(key=image_key)
        images[image_key] = image
    thumb_sizes = list(imageset.__thumbs__)
    for thumb_key, size in thumb_sizes:
        thumbnails[thumb_key] = imageset.get_thumbnail(key=thumb_key)
    return {
        '__type__':'ImageSet',
        'original':original,
        'thumbnails':thumbnails,
        'images':images,
    }

@converter.handle(datetime.datetime)
def convert_datetime(date):
    return {
        '__type__':'datetime',
        'timestamp':timestamp(date),
    }

@converter.handle(datetime.date)
def convert_date(date):
    return {
        '__type__':'datetime',
        'timestamp':timestamp(date),
    }

@converter.handle(dict)
def convert_dict(d):
    converted = {}
    for k, v in d.iteritems():
        converted[converter.convert(k)] = converter.convert(v)
    return converted

@converter.handle(list)
def convert_list(li):
    return map(converter.convert, li)

@converter.handle(formencode.Invalid)
def convert_invalid(invalid):
    return invalid.msg

@converter.handle(f.Form)
def conver_form(form):
    """Convert to default object
    """
    fields = {}
    error_dict = {} if form.error is None else form.error.error_dict
    error_dict = error_dict or {}
    for name in form.fields.iterkeys():
        data = form.form.get(name, None)
        data = converter.convert(data)

        # Field error
        error = error_dict[name] if name in error_dict else None
        error = converter.convert(error)

        field = {
            'data':data,
            'error':error,
        }
        fields[name] = field

    # Schema error
    if (not form.error is None) and (form.error.error_dict is None):
        error = converter.convert(form.error)
    else:
        error = None

    return {
        '__type__':f.__name__ + '.' + type(form).__name__,
        'fields':fields,
        'error':error,
    }

@converter.handle(errors.FormError)
def convert_formerror(form_error):
    return form_error.form

@converter.handle(errors.ResponseError)
def convert_reponseerror(response_error):
    return {
        '__type__':errors.__name__ + '.' + type(response_error).__name__,
        'message':response_error.message,
    }

@converter.handle(FileStorage)
def convert_filestrage(file_storage):
    return None

@converter.default
def convert(obj):
    if callable(getattr(obj, '__default__', None)):
        return obj.__default__()
    raise TypeError('Cannot convert object of ' + repr(type(obj)))


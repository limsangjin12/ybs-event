/******* Events *******/

/* Bottom */
var _bottomListeners = new Array();

function addBottomListener(func) {
    _bottomListeners.push(func);
}

function clearBottomListners() {
    _bottomListeners = new Array();
}

function checkBottom() {
    if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
        for (var i = 0; i < _bottomListeners.length; i++) {
            listner = _bottomListeners[i];
            listner();
        }
    }
}

$(window).scroll(function() {
    checkBottom();
});

$(window).resize(function() {
    checkBottom();
});

$(window).bind("DOMSubtreeModified", function() {
    checkBottom();
});

/* Loading */
var _loadListeners = new Array();
var __loading = false;

function addLoadListener(func) {
    _loadListeners.push(func);
}

function loading(_loading) {
    if (_loading == undefined) {
        return __loading;
    }
	if (__loading != _loading) {
	    __loading = !__loading;
	    for (var i = 0; i < _loadListeners.length; i++) {
            listner = _loadListeners[i];
            listner(__loading);
        }
	}
}

/* Utils */
function getURLArgument(name) {
    var arg = (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
    if (arg == null) {
        return arg;
    }
    return decodeURIComponent(arg);
}

""":mod:`epicl.contrib.ext.sqlalchemy_imageattach_ext`
"""
import io
from ..utils import scale_and_crop
from wand.image import Image as WandImage
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy_imageattach.entity import (
    image_attachment as _image_attachment, ImageSet)
from sqlalchemy_imageattach.context import current_store

class CommonImageSet(ImageSet):
    """Image set for commons.  Provides scaled-images & thumbnails with keys.  
    It negotiates size for scaled image or thumbnail.  
    If worker did not create image yet, it uses larger image that alreay
    exists.  
    """
    __sizes__ = [
        ('large', (1024, 1024)),
        ('medium', (512, 512)),
        ('small', (128, 128)),
    ]
    """Sizes available.  larger-first.  
    Size if limit of width and height. So image can be smaller then written.  
    """

    __thumbs__ = [
        ('large', 500),
        ('medium', 300),
        ('small', 100),
    ]
    """Thumbnails available. larger-first.
    Every thumbnails are perfect squre.  But it can be smaller then written.  
    """

    def thumbnail_size(self, key):
        if self.original is None:
            return None, None
        size = dict(self.__thumbs__)[key]
        size = min(self.original.width, self.original.height, size)
        return size, size

    def image_size(self, key):
        width, height = dict(self.__sizes__)[key]

        if self.original is None:
            return None, None

        origin_width = float(self.original.width)
        origin_height = float(self.original.height)

        if origin_width == 0 or origin_height == 0:
            return 0, 0

        r = width / origin_width
        if origin_height * r > height:
            r = height / origin_height

        width = int(origin_width * r)
        height = int(origin_height * r)

        return width, height

    def get_thumbnail(self, key):
        if self.original is None:
            return None
        thumb_sizes = list(reversed(self.__thumbs__))
        i = 0
        for thumb_key, size in thumb_sizes:
            if key == thumb_key:
                break
            i += 1
        thumb_sizes = thumb_sizes[i:]
        for thumb_key, size in thumb_sizes:
            width, height = self.thumbnail_size(thumb_key)
            try:
                image = self.find_thumbnail(width=width, height=height)
            except NoResultFound as e:
                continue
            return image
        return self.original

    def get_scaled_image(self, key):
        if self.original is None:
            return None
        image_sizes = list(reversed(self.__sizes__))
        i = 0
        for image_key, (width, height) in image_sizes:
            if key == image_key:
                break
            i += 1
        image_sizes = image_sizes[i:]
        for image_key, size in image_sizes:
            width, height = self.image_size(image_key)
            try:
                image = self.find_thumbnail(width=width, height=height)
            except NoResultFound:
                continue
            return image
        return self.original

    def set_original(self, file, store=current_store):
        """Save original image and generate large thumbnail
        """
        self.from_file(file)
        width, height = self.thumbnail_size('large')
        with self.open_file(store=store) as f:
            with WandImage(file=f) as image:
                data = io.BytesIO()
                mimetype = self.original.mimetype
                with image.clone() as large_thumbnail:
                    scale_and_crop(large_thumbnail, width, height)
                    mimetype = large_thumbnail.mimetype
                    large_thumbnail.save(file=data)
                self.from_raw_file(
                    data,
                    store=store,
                    size=(width, height),
                    mimetype=mimetype,
                    original=False
                )


def image_attachment(*args, **kwargs):
    kwargs.setdefault('query_class', CommonImageSet)
    return _image_attachment(*args, **kwargs)

import json
import urllib
from flask import abort
from sqlalchemy import create_engine as _create_engine, orm
from sqlalchemy.types import TypeDecorator, VARCHAR

def engine_url(db_info):
    """Makes URL from `db_info`.  
    """
    q = urllib.quote
    # DB type
    db_url_li = [db_info['DB']]

    # Driver
    if db_info.get('DRIVER', None):
        driver = q(db_info['DRIVER'])
        db_url_li.append('+%s' % driver)
    db_url_li.append('://')

    # User info
    if db_info.get('USER', None):
        user = q(db_info['USER'])
        db_url_li.append(user)

        # With password
        if db_info.get('PASSWORD', None):
            password = q(db_info['PASSWORD'])
            db_url_li.append(':%s' % password)

        # Host
        host = q(db_info.get('HOST', None) or 'localhost')
        db_url_li.append('@%s' % host)

        # Port
        if db_info.get('PORT', None):
            port = db_info['PORT']
            db_url_li.append(':%s' % port)

    # DB Name
    if db_info.get('NAME', None):
        name = db_info['NAME']
        db_url_li.append('/%s' % name)
    return ''.join(db_url_li)

def create_engine(db_info, **kwargs):
    """Creates :mod:`sqlalchemy` engine.  
    """
    if isinstance(db_info, basestring):
        db_url = db_info
    else:
        db_url = engine_url(db_info)
    if not kwargs.has_key('convert_unicode'):
        kwargs['convert_unicode'] = True
    return _create_engine(db_url, **kwargs)

class Query(orm.Query):
    """Customized query.  
    """
    def get_or_404(self, id):
        """Invokes :meth:`get` and aborts 404 if result is None.  
        """
        rv = self.get(id)
        if rv is None:
            abort(404)
        return rv

    def first_or_404(self):
        """Invokes :meth:`first` and aborts 404 if result is None.  
        """
        rv = self.first()
        if rv is None:
            abort(404)
        return rv

    def isexists(self):
        """Get result of query generated by :meth:`exists`.  
        """
        return self.session.query(self.exists()).scalar()

class JSONEncodedDict(TypeDecorator):
    """Represents an immutable structure as a json-encoded string.

    Usage::

            JSONEncodedDict(255)
    """

    impl = VARCHAR

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = json.dumps(value)
        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = json.loads(value)
        return value

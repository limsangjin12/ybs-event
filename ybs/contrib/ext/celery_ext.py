import celery
from functools import wraps
from ..utils.proxy import Proxy

class TaskProxy(Proxy):
    _special_names = list(Proxy._special_names)
    _special_names.remove('__repr__')
    def __init__(self, task, args, kwargs):
        ex = ['task', 'app', 'args', 'kwargs', '_app']
        
        super(TaskProxy, self).__init__(lambda:self.task, exclude_names=ex)
        self.task = task
        self.app = None
        self.args = args
        self.kwargs = kwargs
    
    @property
    def app(self):
        return self._app
    
    @app.setter
    def app(self, app):
        self._app = app
        if app is not None:
            self.task = app.task(*self.args, **self.kwargs)(self.task)
    
    def __str__(self):
        if self.app is None:
            return '<@unbound task: ' + self.task.__name__ + '>'
        else:
            return str(self.task)
    
    def __repr__(self):
        if self.app is None:
            return '<@unbound task: ' + self.task.__name__ + '>'
        else:
            return repr(self.task)

class Blueprint(object):
    """Celery blueprint class.  You can modulize tasks.  For example::
        
        # tasks.py
        from epicl.contrib.ext.celery_ext import Blueprint, Celery
        
        bp = Blueprint('blueprint')
        
        @bp.task()
        def add(x, y):
        return x + y
        
        celery = Celery()
        celery.register_blueprint(bp)
        
        and then you can use it like:
        
        >>> from tasks import add
        >>> add.delay(1, 2)
        
        """
    def __init__(self, name, tasks=None):
        """Initalizer
            :param name: name of blueprint
            """
        self.__name__ = name
        self._tasks = []
        self.tasks = tasks or []

    def add_task(self, fn, args, kwargs):
        task_proxy = TaskProxy(fn, args, kwargs)
        self.tasks.append(task_proxy)
        return task_proxy
    
    def task(self, *args, **kwargs):
        def decorator(fn):
            return self.add_task(fn, args, kwargs)
        return decorator
    
    @property
    def tasks(self):
        return self._tasks
    
    @tasks.setter
    def tasks(self, tasks):
        self._tasks = tasks

class Celery(celery.Celery):
    """Blueprint registerable :class:`celery.Celery`
    """
    def register_blueprint(self, blueprint):
        for task in blueprint.tasks:
            task.app = self

def patch():
    """Patch :class:`~celery.Celery` to support blueprint
    """
    if not celery.Celery is Celery:
        celery.Celery = Celery

class FlaskCelery(Celery):
    def __init__(self, flask_app, *args, **kwargs):
        super(FlaskCelery, self).__init__(*args, **kwargs)
        self.flask_app = flask_app
    
    def register_blueprint(self, blueprint):
        blueprint.flask_app = self.flask_app
        super(FlaskCelery, self).register_blueprint(blueprint)

class FlaskBlueprint(Blueprint):
    """With flask app context
    """
    def task(self, *args, **kwargs):
        def decorater(fn):
            @wraps(fn)
            def wrapper(*args, **kwargs):
                with self.flask_app.app_context():
                    return fn(*args, **kwargs)
            return self.add_task(wrapper, args, kwargs)
        return decorater

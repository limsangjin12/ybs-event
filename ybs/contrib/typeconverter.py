""":mod:`typeconverter`
=======================

Converts object into specified type.  
"""
from functools import wraps

class Handler(object):
    """Converting handler base.  
    """
    def __init__(self, fn, domain):
        super(Handler, self).__init__()
        self.fn = fn
        self.domain = domain
        self.handlable = self.default_handlable

    def matching_type(self, obj):
        """Returns matching type in `domain` if exists, or :const:`None`  
        """
        for t in self.domain:
            if isinstance(obj, t):
                return t
        return None

    def default_handlable(self, obj):
        """Default handlability checker.   
        Just check type of instance.  
        """
        if self.matching_type(obj) is None:
            return False
        return True

    def __call__(self, obj):
        return self.fn(obj)

    def check_handlable(self, fn):
        """Decorator for function that indicates the handler can handle
        object.  
        """
        self.handlable = fn
        return fn

    def can_handle(self, obj):
        return self.handlable(obj)

class Converter(object):
    """Converts object into specified types.  
    """
    def assert_type(self, obj):
        """Asserts if type of `obj` is in range of the converter.  
        """
        for t in self.range:
            if isinstance(obj, t):
                return
        assert False

    def inrange(self, obj):
        """Checks if `obj` is in range of the conveter.  
        """
        try:
            self.assert_type(obj)
        except AssertionError:
            return False
        return True

    def handle(self, *types):
        """Decorator for function that converts type.  
        """
        def decorator(fn):
            handler = Handler(fn, types)
            wraps(fn)(handler)
            self.handlers.append(handler)
            return handler
        return decorator

    def default(self, fn):
        """Decorator that changes default handler.  
        """
        self.default_handler = fn
        return fn

    def find_handler(self, obj):
        """Finds best matching handler.  
        Returns handler whose matching type is most-bottom subclass of class
        hierarchy tree.  
        """
        candidates = []
        for handler in self.handlers:
            if handler.can_handle(obj):
                candidates.append(handler)
        if len(candidates) == 0:
            return None
        best = candidates[0]
        for handler in candidates[1:]:
            best_type = best.matching_type(obj)
            type = handler.matching_type(obj)
            if issubclass(type, best_type):
                best = handler
        return best

    def convert(self, obj):
        """Convert `obj` until it is in `range`.  
        """
        while not self.inrange(obj):
            handler = self.find_handler(obj)
            if handler is None:
                handler = self.default_handler
            obj = handler(obj)
        return obj

    def default_handler(self, obj):
        """Default convert handler.  
        It just raises :class:`TypeError`
        """
        raise TypeError('Cannot convert object of ' + str(type(obj)))

    def __init__(self, range):
        super(Converter, self).__init__()
        if isinstance(range, type):
            range = [range]
        self.range = range
        self.handlers = []

""":mod:`epicl.contrib.utils.model`
===================================

Provides many utils for :mod:`epicl.models`
"""
from flask import g, abort
from sqlalchemy.orm import Query
from sqlalchemy.sql.expression import ClauseElement, or_

def get_object_or_404(model_class, pk, session=None):
    """Returns model indentified by `pk`.  If there's no such a model
    abort 404 by :func:`flask.abort`.  
    :returns: found model
    :rtype: `model_class`
    
    Example::
       
       from epicl.contrib.utils.model import get_object_or_404
       from epicl.models import User

       user = get_object_or_404(User, pk=1) # 1 exists, succeed
       user = get_object_or_404(User, pk=2) # not exists, fail
    """
    session = session or g.session
    obj = session.query(model_class).filter(model_class.id==pk).first()
    if obj is None:
        abort(404)
    return obj

def get_or_create(model, defaults=None, session=None, **kwargs):
    """Get model with options. If such a model not exists, create model with 
    options.

    :returns: model and boolean value that indicates created bound by tuple
        (model, created)
    :rtype: :class:`tuple`
    """
    session = session or g.session
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance, False
    else:
        params = dict((k, v) for k, v in kwargs.iteritems()
            if not isinstance(v, ClauseElement))
        if defaults:
            params.update(defaults)
        instance = model(**params)
        session.add(instance)
        return instance, True

def get_pk(model):
    """Finds primary key of `model`.  

    :param model: model class.  

    :returns: primary key field if found, or :const:`None`.  
    """
    pk_fields = filter(lambda x:x.primary_key, model.__table__.columns)
    for pk_field in pk_fields:
        pk_field = getattr(model, pk_field.name, None)
        if pk_field is None:
            continue
        return pk_field
    return None

def slice_query(query, order_by, model, max_len=None, last_id=None, desc=True):
    """Adds slice statements for query and returns results.  

    :param query: pre-processed query.  
    :param order_by: field for used by ordering.  
    :param model: target model.  
    :param max_len: maximum size for result.  :const:`None` to unlimited.  
    :param last_id: last object's primary key, :const:`None` to no last object.
    :param desc: indicates that query is descending.  

    :returns: sliced query results
    """
    pk = get_pk(model)
    # From last item
    if not last_id is None:
        last_query = Query(order_by).filter(pk==last_id)
        if desc:
            query = query.filter(order_by <= last_query)
            query = query.filter(or_(
                order_by != last_query,
                pk < last_id
            ))
        else:
            query = query.filter(order_by >= last_query)
            query = query.filter(or_(
                order_by != last_query,
                pk > last_id
            ))
    # Apply Order
    if desc:
        query = query.order_by(order_by.desc())
        query = query.order_by(pk.desc())
    else:
        query = query.order_by(order_by)
        query = query.order_by(pk)

    # Slice
    if max_len is None:
        return query.all()
    else:
        return query[:max_len]

from flask import g

class SQLAlchemyMiddleware(object):
    def __init__(self, session, app=None):
        self.app = app
        self.session = session
        if self.app is not None:
            self.init_app(app)

    def init_app(self, app):
        @app.before_request
        def before_request():
            g.session = self.session()

        @app.teardown_request
        def teardown_request(exception):
            if hasattr(g, 'session'):
                if not exception is None:
                    g.session.rollback()
                g.session.close()

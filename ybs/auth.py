# -*- encoding: utf-8 -*-
import models as m
import forms as f
from flask import session, g, Blueprint, abort, request, redirect, url_for
from render import render, wants_html
from errors import FormError
from gettext import gettext as _
from functools import wraps
from contrib.utils.form import set_form_error

def authenticate(username, password):
    user = m.User.query.filter_by(username=username).first()
    if user is None or not user.check_password(password):
        return None
    return user

def login(user):
    session['user_id'] = user.id

def logout(user):
    session.pop('user_id', None)

def get_login_user():
    user_id = session.get('user_id', None)
    if user_id:
        return m.User.query.filter_by(id=user_id).first()
    auth = request.authorization
    if auth:
        return authenticate(auth.username, auth.password)
    return None
def abort_login():
    if wants_html():
        return redirect(url_for('auth.login', next=request.full_path))
    abort(401)

def login_required(fn):
    @wraps(fn)
    def decorator(*args, **kwargs):
        if getattr(g, 'user', None) is None:
            return abort_login()
        return fn(*args, **kwargs)
    return decorator

auth_blueprint = Blueprint('auth', __name__)

@auth_blueprint.route('/login', methods=['GET', 'POST'], endpoint='login')
def login_view():
    """로그인합니다.  

    :statuscode 200: 로그인에 성공하였습니다.  로그인된 User를 반환합니다.  
    :statuscode 400: 잘못된 form을 입력했습니다. 에러가 포함된 Form을
        반환합니다.

    :query next: 로그인이 성공할 시 redirect될 URL. 

    * :class:`epicl.forms.LoginForm` 에서 request의 form을 확인하세요.  
    """
    form = f.LoginForm()
    if form.validate(template='auth/login'):
        data = form.data
        username = data['username']
        password = data['password']
        user = authenticate(username, password)
        if user is None:
            set_form_error(form, _('Invalid informations.'))
            raise FormError(form, template='auth/login')
        login(user)
        next = request.args.get('next', None)
        if next is None:
            if wants_html():
                return redirect(url_for('main.main'))
            return render(user, status=200)
        return redirect(next)
    return render(form, template='auth/login')

@auth_blueprint.route('/logout', methods=['GET'], endpoint='logout')
def logout_view():
    """로그아웃합니다.  

    :statuscode 200: 로그인에 성공하였습니다.  
    :statuscode 401: 로그인이 되어있지 않습니다.  
    """
    if not g.user:
        if wants_html():
            return redirect(url_for('auth.login'))
        return abort_login()
    logout(g.user)
    if wants_html():
        return redirect(request.referrer or url_for('main.main'))
    return render('Done', status=200)

@auth_blueprint.route('/me')
@login_required
def me():
    """로그인된 User를 반환합니다.  

    :statuscode 200: 로그인된 User를 반환합니다.  
    :statuscode 401: 로그인이 필요합니다.  
    """
    return render(g.user)

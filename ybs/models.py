import io
import math
import ctypes
import hashlib
import sqlalchemy as s
from flask import g, abort
from wand.api import library
from wand.image import Image as WandImage
from wand.color import Color
from wand.drawing import Drawing
from sqlalchemy import func
from sqlalchemy.sql import exists
from sqlalchemy.sql.expression import and_
from sqlalchemy.orm import relationship, scoped_session, sessionmaker
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_imageattach.entity import Image
from sqlalchemy_imageattach.context import current_store
from contrib.utils import is_integer, scale_and_crop
from contrib.ext.sqlalchemy_ext import Query
from contrib.ext.sqlalchemy_imageattach_ext import (CommonImageSet, 
    image_attachment as _image_attachment)

def rgb_to_hsv(r, g, b):
    min_value = min(r, g, b)
    max_value = max(r, g, b)
    v = max(r, g, b)
    delta = max_value - min_value
    if delta == 0:
        h = 0
        s = 0
    else:
        s = delta / max_value

        delta_r = (((max_value - r) / 6) + (delta / 2)) / delta
        delta_g = (((max_value - g) / 6) + (delta / 2)) / delta
        delta_b = (((max_value - b) / 6) + (delta / 2)) / delta

        if (r == max_value):
            h = delta_b - delta_g
        elif (g == max_value):
            h = 1.0 / 3 + delta_r - delta_b
        elif (b == max_value):
            h = 2.0 / 3 + delta_g - delta_r

        if h < 0:
            h += 1
        if h > 1:
            h -= 1
    return h, s, v

def hsv_to_rgb(h, s, v):
    if s == 0:
        return v, v, v
    h *= 6
    if h == 6:
        h = 0
    i = math.floor(h)
    v1 = v * (1 - s)
    v2 = v * (1 - s * (h - i))
    v3 = v * (1 - s * (1 - (h - i)))

    if i == 0:
        r = v
        g = v3
        b = v1
    elif i == 1:
        r = v2
        g = v
        b = v1
    elif i == 2:
        r = v1
        g = v
        b = v3
    elif i == 3:
        r = v1
        g = v2
        b = v
    elif i == 4:
        r = v3
        g = v1
        b = v
    else:
        r = v
        g = v1
        b = v2
    return r, g, b

class YBSImageSet(CommonImageSet):
    __sizes__ = ()
    __thumbs__ = (
        ('large', 500),
        ('small', 32),
    )

    def make_thumbnail(self, key, store=current_store):
        width, height = self.thumbnail_size(key)
        with self.open_file(store=store) as f:
            with WandImage(file=f) as image:
                data = io.BytesIO()
                mimetype = self.original.mimetype
                with image.clone() as thumbnail:
                    scale_and_crop(thumbnail, width, height)
                            
                    mimetype = thumbnail.mimetype
                    thumbnail.save(file=data)
                self.from_raw_file(
                    data,
                    store=store,
                    size=(width, height),
                    mimetype=mimetype,
                    original=False
                )

    def set_original(self, file, store=current_store):
        """Save original image and generate large thumbnail
        """

        # Fix orientation
        with WandImage(file=file) as image:
            data = io.BytesIO()
            orientation = image.metadata.get('exif:Orientation', None)
            if not orientation is None:
                orientation = int(orientation)
                if orientation == 2:
                    image.flop()
                elif orientation == 3:
                    image.rotate(180)
                elif orientation == 4:
                    image.flip()
                elif orientation == 5:
                    image.rotate(90)
                    image.flop()
                elif orientation == 6:
                    image.rotate(90)
                elif orientation == 7:
                    image.rotate(90)
                    image.flip()
                elif orientation == 8:
                    image.rotate(270)
                key = b'exif:Orientation'
                v = library.MagickSetImageProperty(image.wand, key, b'1')
                if not bool(v):
                    print 'Set image property failure'
                library.MagickSetImageOrientation(image.wand, 1)
            mimetype = image.mimetype
            size = image.size

            image.save(file=data)
            self.from_raw_file(
                data,
                store=store,
                size=size,
                mimetype=mimetype,
                original=True,
            )
        self.make_thumbnail('large')

        # Small thumb
        width, height = self.thumbnail_size('small')
        with self.open_file(store=store) as f:
            with WandImage(file=f) as image:
                data = io.BytesIO()
                mimetype = self.original.mimetype
                with image.clone() as thumbnail:
                    scale_and_crop(thumbnail, width, height)
                    pixels = []
                    for row in thumbnail:
                        row_pixels = []
                        for color in row:
                            r = color.red
                            g_ = color.green
                            b = color.blue
                            a = color.alpha
                            h, s, v = rgb_to_hsv(r, g_, b)
                            s *= 0.5
                            r, g_, b = hsv_to_rgb(h, s, v)
                            row_pixels.append((r, g_, b, a))
                        pixels.append(row_pixels)
                with Drawing() as draw:
                    with WandImage(width=width, height=height) as img:
                        for i in range(width):
                            for j in range(height):
                                r, g_, b, a = [
                                    int(x * 255)
                                    for x in pixels[i][j]
                                ]
                                string = ''
                                string += '%02s' % hex(r)[2:]
                                string += '%02s' % hex(g_)[2:]
                                string += '%02s' % hex(b)[2:]
                                string = string.replace(' ', '0')
                                string = '#' + string
                                with Color(string) as c:
                                    draw.fill_color = c
                                    draw.line((j, i), (j, i))
                                    draw(img)
                        img.format = 'jpeg'
                        mimetype = img.mimetype
                        img.save(file=data)
                self.from_raw_file(
                    data,
                    store=store,
                    size=(width, height),
                    mimetype=mimetype,
                    original=False
                )

def image_attachment(*args, **kwargs):
    kwargs.setdefault('query_class', YBSImageSet)
    return _image_attachment(*args, **kwargs)

Base = declarative_base()

def make_password(raw):
    md5 = hashlib.md5()
    md5.update(raw)
    return md5.hexdigest()

class Group(Base):
    """User group.  
    """
    __tablename__ = 'group'
    
    id = s.Column(s.Integer, primary_key=True)
    MAX_NAME_LEN=255
    name = s.Column(s.String(MAX_NAME_LEN), unique=False)
    is_admin = s.Column(s.Boolean, default=False, nullable=False)

    def has_permission(self, perm_name):
        if self.is_admin:
            return True
        session = best_session(self)
        perm_id, = session.query(Permission.id).filter_by(
            name=perm_name
        ).first()

        if perm_id is None:
            return False

        return session.query(
            exists().where(and_(
                GroupPermission.group_id == self.id,
                GroupPermission.permission_id == perm_id,
            ))
        ).scalar()

class Permission(Base):
    __tablename__ = 'permission'

    id = s.Column(s.Integer, primary_key=True)
    MAX_NAME_LEN=255
    name = s.Column(s.String(MAX_NAME_LEN), nullable=False, unique=True)
    description = s.Column(s.Text, nullable=True)

class GroupPermission(Base):
    __tablename__ = 'group_permission'
    __table_args__ = (UniqueConstraint('group_id', 'permission_id'), )

    id = s.Column(s.Integer, primary_key=True)

    group_id = s.Column(s.Integer,
        s.ForeignKey('group.id', ondelete='CASCADE'), nullable=False)
    group = relationship('Group',
        primaryjoin='GroupPermission.group_id==Group.id')
    
    permission_id = s.Column(s.Integer,
        s.ForeignKey('permission.id', ondelete='CASCADE'), nullable=False)
    permission = relationship('Permission',
        primaryjoin='GroupPermission.permission_id==Permission.id')

class User(Base):
    """User ORM Model.  

    Serialization form is :class:`dict` with following keys.  

    __type__:
        :type: :class:`str`

        ``'epicl.models.User'``

    id:
        :type: :class:`int`

        Primary key

    group_id:
        :type: :class:`int`

        ID of :class:`Group` user involved in.  

    username:
        :type: :class:`str`

    create_date:
        :type: :class:`datetime.datetime`

                You can find serializable form of this class at
                :doc:`../serializable`.

        User's creation time.  
    """
    __tablename__ = 'user'
    __with__ = ('location', 'photo')
    __ex__ = ('password', )

    id = s.Column(s.Integer, primary_key=True)

    group_id = s.Column(s.Integer,
        s.ForeignKey('group.id', ondelete='SET NULL'), nullable=True)
    group = relationship('Group',
        primaryjoin='User.group_id==Group.id')

    # Password definitions
    MAX_PASSWORD_LEN=128
    _password = s.Column('password', s.String(MAX_PASSWORD_LEN), nullable=True)

    @hybrid_property
    def password(self):
        """User's password
        """
        return self._password

    @password.setter
    def password(self, password):
        """Setter of :data:`password`.  It automatically hashes raw password.  

        :param password: raw password string.  
        """
        if password is None:
            self._password = None
            return
        if password == '':
            hashed = ''
        else:
            hashed = make_password(password)
        self._password = hashed

    @password.expression
    def password(self):
        return User._password

    def check_password(self, password):
        """Checks password.  

        :param password: raw password string.  
        """
        return self.password == make_password(password)

    MAX_NAME_LEN=30
    username = s.Column(s.String(MAX_NAME_LEN), nullable=False, unique=True)
    create_date = s.Column(s.DateTime, nullable=False, default=func.now())

    def has_permission(self, permission):
        if self.group is None:
            return False
        return self.group.has_permission(permission)

class Card(Base):
    """Card uploaded by user
    """
    __tablename__ = 'card'
    __with__ = ('photo', )
    id = s.Column(s.Integer, primary_key=True)
    name = s.Column(s.String(20), nullable=False)
    message = s.Column(s.Text, nullable=False)
    photo = image_attachment('Photo')
    create_date = s.Column(s.DateTime, nullable=False, default=func.now())
    is_confirmed = s.Column(s.Boolean, default=False, nullable=False)
    is_rejected = s.Column(s.Boolean, default=False, nullable=False)

class Photo(Base, Image):
    __tablename__ = 'photo'
    
    card_id = s.Column(s.Integer, s.ForeignKey('card.id', ondelete='CASCADE'),
        primary_key=True)
    card = relationship('Card', primaryjoin='Photo.card_id==Card.id')

    @property
    def object_id(self):
        return self.card_id

session_factory = sessionmaker(query_cls=Query)
Session = scoped_session(session_factory)

# Query Property
Base.query = Session.query_property()

def create_tables():
    Base.metadata.create_all()

def drop_tables():
    Base.metadata.reflect()
    Base.metadata.drop_all()

def best_session(model=None):
    """Chooses best sqlalchemy session of model.  
    :param model: model object or :const:`None`
    :returns: session
    """
    session = None
    if not model is None:
        session = Session.object_session(model)
    if not session is None:
        return session
    session = getattr(g, 'session', None)
    if not session is None:
        return session
    session = Session()
    if not model is None:
        session.add(model)
    return session

# Useful functions
def get_user(heroname_or_id):
    from auth import abort_login
    if heroname_or_id == 'me':
        if getattr(g, 'user', None) is None:
            abort_login()
        return g.user
    if isinstance(heroname_or_id, int) or is_integer(heroname_or_id):
        user_id = int(heroname_or_id)
        return User.query.filter_by(id=user_id).first()
    else:
        return User.query.filter_by(heroname=heroname_or_id).first()

def get_user_or_404(heroname_or_id):
    user = get_user(heroname_or_id)
    if user is None:
        abort(404)
    return user

def init_admin():
    session = Session()

    # Admin group
    group = Group(name='Admin', is_admin=True)
    session.add(group)

    # Admin user
    user = User(
        username='admin',
        password='Admin',
        group=group,
    )
    session.add(user)

    session.commit()
    session.close()
    
def init_defaults():
    """Inserts default models
    """
    init_admin()

def rebuild_db():
    """Drop and Re-create table and insert default models.  
    """
    drop_tables()
    create_tables()
    init_defaults()

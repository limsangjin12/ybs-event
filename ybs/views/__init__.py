from main_view import main_blueprint
from card_view import card_blueprint

def register_views(app):
    app.register_blueprint(main_blueprint)
    app.register_blueprint(card_blueprint)

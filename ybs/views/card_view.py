from .. import models as m
from .. import forms as f
from ..render import render, wants_html
from ..auth import login_required
from flask import Blueprint, g, abort, redirect, url_for, request
from flask.ext.negotiation import provides
from ..contrib.utils.model import slice_query, get_object_or_404

card_blueprint = Blueprint('card', __name__)

@card_blueprint.route('/card/create', methods=['GET'])
def create_form():
    form = f.CardForm()
    return render(form, template='card/create')

@card_blueprint.route('/card/', methods=['POST'])
@card_blueprint.route('/card/create', methods=['POST'])
def create():
    form = f.CardForm()
    form.validate(template='card/create')
    data = form.data

    name = data['name']
    message = data ['message']
    photo = data['photo']

    card = m.Card(
        name=name,
        message=message,
    )
    card.photo.set_original(photo)
    g.session.add(card)
    g.session.commit()
    return render(card, status=201, template='card/uploaded')

@card_blueprint.route('/card/')
@login_required
def index():
    cards = m.Card.query.all()
    return render(cards, template='card/index')

@card_blueprint.route('/card/<int:card_id>')
@login_required
def read(card_id):
    if not g.user.has_permission('card-read'):
        abort(403)
    card = get_object_or_404(m.Card, card_id)
    return render(card, template='card/read')

@card_blueprint.route('/card/<int:card_id>', methods=['DELETE'])
@login_required
def delete(card_id):
    if not g.user.has_permission('card-delete'):
        abort(403)
    card = get_object_or_404(m.Card, card_id)
    g.session.delete(card)
    g.session.commit()
    if wants_html():
        return redirect(url_for('card.index'))
    return render(None, status=204)

@card_blueprint.route('/card/confirmed')
@login_required
def confirmed():
    q = m.Card.query.filter_by(is_confirmed=True, is_rejected=False)
    q = q.order_by(m.Card.create_date)
    cards = slice_query(q, m.Card.create_date, m.Card, desc=False,
        last_id=request.args.get('last_id', None))
    return render(cards, template='card/confirmed_index')

@card_blueprint.route('/card/<int:card_id>/confirm', methods=['GET', 'POST'])
@login_required
def confirm(card_id):
    if not g.user.has_permission('card-confirm'):
        abort(403)
    card = get_object_or_404(m.Card, card_id)
    if request.method == 'POST':
        g.session.add(card)
        card.is_confirmed = True
        g.session.commit()
        return redirect(url_for('card.confirm_next'))
    return render(card, template='card/confirm')

@card_blueprint.route('/card/<int:card_id>/reject', methods=['GET', 'POST'])
@login_required
def reject(card_id):
    if not g.user.has_permission('card-confirm'):
        abort(403)
    card = get_object_or_404(m.Card, card_id)
    if request.method == 'POST':
        g.session.add(card)
        card.is_rejected = True
        g.session.commit()
        return redirect(url_for('card.confirm_next'))
    return render(card, template='card/confirm')

@card_blueprint.route('/card/confirm_next', methods=['GET', 'POST'])
@login_required
def confirm_next():
    card = m.Card.query.filter_by(
        is_confirmed=False,
        is_rejected=False
    ).order_by(
        m.Card.create_date
    ).first()
    if card is None:
        return render(None, template='card/nothing')
    return redirect(url_for('card.confirm', card_id=card.id))

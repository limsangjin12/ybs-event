from flask import Blueprint
from ..render import render

main_blueprint = Blueprint('main', __name__)

@main_blueprint.route('/')
def main():
    """Main page
    """
    msg = 'Don\'t worry. Namjun is impotent!'
    return render(msg, template='main')
